
# Privacy Policy - Gameoteca
##Introduction
This is the Privacy Policy of Gamoteca. Gamoteca is a learning framework that allows trainers to rapidly create and launch real-life collaborative games through a mobile app. In this Privacy Policy, we refer to our applications (mobile and web) and services as the �Application�. Please read on to learn more about our data handling practices. Your use of the Application signifies that you agree with the terms of this Privacy Policy. If you do not agree with the terms of this Privacy Policy, do not use the Application.

##Information Collected and User Content
When you register with us and use the Application, you generally provide (a) your name, email address, username, password and other registration information; (b) information you provide us when you contact us for help; (c) information you enter into our system when using the Application, such as user content (e.g., photos, videos, text, and other materials) that you create or import on the Application. Please note that in order to upload user content into the system you will need to provide the Application access to your camera, gallery and microphone. 
When you install the Application on your device and you sign in, we collect the following types of information from you: (a) geolocation Information, (b) device information.
By choosing to share that information, you should understand that you may no longer be able to control how that information is used and that it may become publicly available (depending in part on your actions or the actions of others with whom you have shared the information). We are not responsible for any use or misuse of information you share. 
We use technologies like cookies (small files stored by your browser), web beacons, or unique device identifiers to anonymously identify your computer or device so we can deliver a better experience. Our systems also log information like your browser, operating system and IP address.
We do not knowingly contact or collect personal information from children under 18. If you believe we have inadvertently collected such information, please contact us so we can promptly obtain parental consent or remove the information.

##How We Use Collected Information 
Your personal information, automatically collected information or user content may be used or disclosed only as follows:

*  By the game administrator to assign you to a particular game and team;  
*  To connect you with others as enabled by the Application;  
*  To share your profile with others on the Application;  
*  To show you the names of persons you communicate with and to show your name to persons you communicate with on the Application;  
*  To deliver to you any administrative notices, alerts and communications relevant to your use of the Application;  
*  To provide you with relevant content that you requested, using information that you allow us to collect from you, such as information regarding your and your Application contacts' respective locations;  
*  For internal operations, including troubleshooting problems, data analysis, testing, research, improvements to the Application, detecting and protecting against error, fraud or other illegal activity;  
*  With our trusted services providers who work on our behalf, do not have an independent use of the information we disclose to them and have agreed to adhere to the rules set forth in this privacy statement;  
*  When we have a good faith belief that the law, any legal process, law enforcement, national security or issue of public importance requires disclosure, such as to comply with a subpoena, or similar legal process;  
*  When we believe in good faith that disclosure is necessary to protect our rights or property, protect your safety or the safety of others, investigate fraud, or respond to a government request;  
*  If Gamoteca is involved in a merger, acquisition, or sale of all or a portion of its assets, you will be notified via email and/or a prominent notice on our website of any change in ownership or uses of this information, as well as any choices you may have regarding this information.  

##Sharing of Information and Third Party Links
We will not trade, rent or sell your information to third parties. Only aggregated, anonymised data is periodically collected to improve the Application and our service. Any information or content that you voluntarily disclose for posting to the Application, such as user content, becomes available on our web platform. Once you have shared user content or made it public, that user content may be viewed and re-shared by others who have access to the web platform.
Your information may be stored and processed in any country in which Application maintains facilities. In this regard, or for purposes of sharing or disclosing data as described in this Privacy Policy, we reserve the right to transfer information outside of your country. By using the Application, you consent to any such transfer of information outside of your country.
We generally do not share personally identifiable information (such as name, username, email) with other companies. We do not allow advertising companies to collect data through our service for ad targeting. Occasionally, at our discretion, we may include or offer third party products or services in our Applications. These third party products have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked applications. Nonetheless, we seek to protect the integrity of our applications and welcome any feedback about these applications.

##Notification of Changes
We reserve the right at our discretion to make changes to this Privacy Policy. You may review updates to our Privacy Policy at any time via links on Google Play/Apple Store. We reserve the right to modify this policy from time to time, so please review it frequently.
If we make material changes to this policy, we will notify you by publishing a revised Privacy Policy or as required by law. You agree to review the Privacy Policy periodically so that you are aware of any modifications. You agree that your continued use of the Application after we publish a revised Privacy Policy or provide a notice on our applications constitutes your acceptance of the revised Privacy Policy. If you do not agree with the terms of the Privacy Policy, you should not use the Application.

##Security 
We are concerned about safeguarding the confidentiality of your information. We provide physical, electronic, and procedural safeguards to protect information we process and maintain. For example, we limit access to this information to authorized employees and contractors who need to know that information in order to operate, develop or improve our Application. Please be aware that, although we endeavor to provide reasonable security for information we process and maintain, no security system can prevent all potential security breaches.

##Opt-Out Rights
You can stop all collection of information by the Application easily by uninstalling the Application. You may use the standard uninstall processes as may be available as part of your mobile device or via the mobile application marketplace or network. If you�d like us to delete user data that you have provided via the Application, please contact us at info@gamoteca.com. We remove personally identifiable information (such as your name and email) promptly after you delete your account. We may retain other data indefinitely.

##Contact Information
If you have any questions about this Privacy Policy, please contact us at info@gamoteca.com. Any personally identifiable information provided in connection with inquiries related to this Privacy Policy will be used solely for the purpose of responding to the inquiry and consistently with this Privacy Policy.